# Hardware acceleration of Multiple Constant Multiplication Problem on FPGAs



#### table of content
- [Description](#description)
- [How to build](#how-to-build)

#### Description

This project aims to create an Hardware accelerator for the Multiple Constant Multiplication problem. This accelerator implement a depth-first search algorithm by Levent Askoy, described in Aksoy, L., Güneş, E. O., &#38; Flores, P. (2010). Search algorithms for the multiple constant multiplications problem: Exact and approximate. <i>Microprocessors and Microsystems</i>, <i>34</i>(5), 151–162. https://doi.org/10.1016/j.micpro.2009.10.001.
The design of the accelerator is made with High Level Synthesis on [Vitis](https://www.xilinx.com/products/design-tools/vitis/vitis-platform.html) and the experiments are made on an Alveo U280.
To evaluate our design, we made a software version of this algorithm to compare with it.

#### How to build.

##### How to build and use the software version.

This project use `CMake` with a custom `Makefile` to have every build file in a `build/` folder. You can initialize the build system with the command `make cmake` and then use the command `make` to build the project. It creates an executable named `dfs`.
The command line arguments to give to this executable are the constants the multiplier block have to multiply his inputs  by and the result of this program give the smallest set extended with intermediate constants to compute the multiplication with only shift, addition and subtractions.
