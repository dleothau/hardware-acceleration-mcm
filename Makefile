CMakeContent="""cmake_minimum_required( VERSION 3.12 )\n\
set(CMAKE_BUILD_TYPE Release)\n\
project (project)\n\
\n\
file(GLOB SRC CONFIGURE_DEPENDS \"hdr/*.h\" \"src/*.c\" \"hdr/*.hpp\" \"src/*.cpp\")\n\
add_compile_options(-O3 -fconcepts -std=gnu++2a)\n\
add_link_options()\n\
include_directories(hdr/)\n\
add_executable(dfs \$${SRC})\n\
"""

all:
	@$(MAKE) -Cbuild/
	@cp build/dfs dfs

clean:
	@rm -rf build/
	@rm -f dfs

cmake:
	@echo $(CMakeContent) > CMakeLists.txt
	@mkdir -p src/
	@mkdir -p hdr/
	@mkdir -p build/
	@cmake -Bbuild/ -H.
	
mrproper:
	@rm -rf build/
	@rm -f dfs
	@rm -f CMakeLists.txt
