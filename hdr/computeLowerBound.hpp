#include "realisationSet.hpp"

#ifndef COMPUTE_LOWER_BOUND_HPP_
#define COMPUTE_LOWER_BOUND_HPP_


uint32_t compute_lower_bound(realisation_set_t& R, target_set_t& T);
#endif
