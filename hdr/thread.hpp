#include "realisationSet.hpp"
#include "targetSet.hpp"

#include <stack>

#ifndef THREAD_HPP_
#define THREAD_HPP_

#define NUM_THREAD 8
#define NUM_INIT 15

typedef struct
{
  realisation_set_t R;
  target_set_t T;
  int last_ic;
} stack_elt_t;

typedef std::stack<stack_elt_t*> stack_t;

real_set_t multiThread(target_set_t T);

#endif
