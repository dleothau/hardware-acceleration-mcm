#include "realisationSet.hpp"

#ifndef EVALUATE_COST_HPP_
#define EVALUATE_COST_HPP_

uint32_t evaluate_cost(target_set_t& B);

#endif
