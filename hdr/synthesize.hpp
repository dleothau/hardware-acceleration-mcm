#include "realisationSet.hpp"

#ifndef SYNTHESIZE_HPP_
#define SYNTHESIZE_HPP_

void synthesize(realisation_set_t& R, target_set_t& T);

bool synthesize_no_change(realisation_set_t& R, target_set_t& T);

#endif
