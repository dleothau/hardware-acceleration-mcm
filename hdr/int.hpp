#include <stdint.h>

#ifndef INT_HPP_
#define INT_HPP_

extern uint32_t c_max, bw;

uint64_t popcount(uint64_t x);
uint32_t popcount(uint32_t x);

uint32_t log2c(uint32_t x);
uint32_t log2f(uint32_t x);
uint32_t odd(uint32_t x);

int compute_k_max(uint32_t u, uint32_t v);


#endif
