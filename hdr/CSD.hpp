#include <stdint.h>

#ifndef CSD_HPP_
#define CSD_HPP_

#define MAX_COMPUTED (1<<19)

uint32_t number_non_zero_digit(uint32_t x);

#endif
