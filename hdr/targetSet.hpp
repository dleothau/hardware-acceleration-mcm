#include <unordered_set>
#include <vector>

#include "int.hpp"

#ifndef TARGET_SET_HPP_
#define TARGET_SET_HPP_

typedef std::unordered_set<uint32_t> targ_set_t;

class target_set_t
{
protected:
  targ_set_t set;

public:
  
  target_set_t(void) {}
  target_set_t(targ_set_t s) { this->set = s; }
  target_set_t(target_set_t& other) { this->set = other.set; }
  
  void insert(uint32_t x);
  
  targ_set_t::iterator erase(targ_set_t::iterator it);
  void erase(uint32_t x);

  bool contains(uint32_t x);
  bool empty(void);
  size_t size(void);

  targ_set_t::iterator begin(void);
  targ_set_t::iterator end(void);

  target_set_t& operator=(target_set_t& other);

};

extern target_set_t g_target;

#endif
