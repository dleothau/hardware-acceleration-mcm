#include <stdint.h>

#ifndef SCC_HPP_
#define SCC_HPP_

uint32_t single_coefficient_cost(uint32_t b);

#endif
