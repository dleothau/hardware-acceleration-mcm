#include "realisationSet.hpp"

#ifndef APPROXIMATE_SEARCH_HPP_
#define APPROXIMATE_SEARCH_HPP_

real_set_t approximate_search(target_set_t T);

#endif
