#include "realisationSet.hpp"

#ifndef DF_SEARCH_HPP_
#define DF_SEARCH_HPP_

real_set_t DFSearch(target_set_t T, uint32_t depth);

#endif
