#include <map>

#include "int.hpp"
#include "targetSet.hpp"

#ifndef REALISATION_SET_HPP_
#define REALISATION_SET_HPP_

#define MAX_PROF_SYMMETRY_CHECK 10

typedef std::vector<uint32_t> real_set_t;

class realisation_set_t
{
public:
  real_set_t set;
  real_set_t successors;
  real_set_t tmp_suc;
  
  void add_successor(uint32_t x);
  void add_successor(uint32_t x, real_set_t& tmp);  
  void add_successor(uint32_t x, realisation_set_t& other);
  
  realisation_set_t(void) {}
  realisation_set_t(realisation_set_t& other)
    {
      this->set = other.set;
      this->successors = other.successors;
      this->tmp_suc = other.tmp_suc;
    }
  realisation_set_t(real_set_t& set, real_set_t& succ)
    {
      this->set = set;
      this->successors = succ;
    }
  void insert(uint32_t x);
  void insert(uint32_t x, realisation_set_t& other);
  
  void erase(uint32_t x);
  
  bool empty(void);
  bool contains(uint32_t x);
  bool successor(uint32_t x);
  size_t size(void);

  real_set_t::iterator begin(void);
  real_set_t::iterator end(void);

  real_set_t::iterator begin_successor(void);
  real_set_t::iterator end_successor(void);

  realisation_set_t& operator=(realisation_set_t& other);

  real_set_t remove_redundant(void);

  target_set_t to_target(void);

  uint32_t find_intermediate_constant(uint32_t ic, int d);
  uint32_t find_intermediate_constant(uint32_t ic);
  uint32_t find_intermediate_constant_symmetry_check(uint32_t ic);

};

realisation_set_t get_one(void);

#endif
