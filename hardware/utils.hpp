#ifndef UTILS_HPP_
#define UTILS_HPP_

int log2f(int _x);
int log2c(int _x);
int odd(int x);
int compute_k_max(int c_max, int u, int v);

#endif
