#include "sort.hpp"

void sort(unsigned int (&S)[MAX_S_SIZE], int size)
{
#pragma HLS inline
	/* http://sweet.ua.pt/iouliia/Courses/PDP_TUT/TUT_2015_T2.pdf */
	if (size < 2)
		return;

	unsigned int even[MAX_S_SIZE];
#pragma HLS array_partition variable=even complete

	for (int i = 0; i < MAX_S_SIZE; i++)
#pragma HLS unroll
		S[i] = ((i < size) ? S[i] : ~((unsigned int)0));

	bool sorting_completed = false;

	for (int i = 0; i < MAX_S_SIZE; i++)
	{
#pragma HLS unroll
		if (sorting_completed)
			break;
		sorting_completed = true;
		even[0] = (S[0] > S[1]) ? S[1] : S[0];
		even[1] = (S[0] > S[1]) ? S[0] : S[1];
		sorting_completed &= !(S[0] > S[1]);
		for (unsigned j = 1; j < MAX_S_SIZE/2; j++)
		{
#pragma HLS unroll
			bool cond1 = (S[2*j] > S[2*j+1]);
			unsigned int tmp = cond1 ? S[2*j+1] : S[2*j];
			bool cond2 = (even[2*j-1] > tmp);

			even[2*j] = tmp;
			even[2*j+1] = cond1 ? S[2*j] : S[2*j+1];

			S[2*j-1] = cond2 ? tmp : even[2*j-1];
			S[2*j] = cond2 ? even[2*j-1] : tmp;

			sorting_completed &= !(cond1 | cond2);
		}
		S[0] = even[0];
		S[MAX_S_SIZE-1] = even[MAX_S_SIZE-1];
	}

}
