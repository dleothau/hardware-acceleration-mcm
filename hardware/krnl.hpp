#include <ap_int.h>

#ifndef KRNL_HPP_
#define KRNL_HPP_

#define MAX_BW 15
#define MAX_DEPTH 23
#define MAX_TARGET 10

#define BRAM 32

typedef ap_uint<1> bit_t;
typedef ap_int<3+MAX_BW> int_t;

#define SHIFTED_OR(n,x) ((x)|(x)>>n)
#define FIRST_POWER_OF_TWO(x) (1+                                  \
				SHIFTED_OR(16,                                     \
					SHIFTED_OR(8,                                  \
							SHIFTED_OR(4,                          \
									SHIFTED_OR(2,                  \
											SHIFTED_OR(1,x))))))

#define MAX_INT (1<<(MAX_BW-1))
#define MAX_S_SIZE FIRST_POWER_OF_TWO(1+MAX_DEPTH+MAX_TARGET)
#define MAX_R_SIZE (1 + MAX_DEPTH + MAX_TARGET)
#define NEW_SUCC_SIZE (4*MAX_BW-2)

const int _MAX_BW = MAX_BW;
const int _MAX_DEPTH = MAX_DEPTH;
const int _MAX_INT = MAX_INT;
const int _MAX_TARGET = MAX_TARGET;
const int _MAX_S_SIZE = MAX_S_SIZE;
const int _BRAM = BRAM;


#endif
