#include "krnl.hpp"

#include "synthesize.hpp"
#include "add_successors.hpp"
#include "FIC.hpp"
#include "CLB.hpp"


void copy(int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int_t (&WT)[MAX_DEPTH][MAX_TARGET],
		int (&Rsize)[MAX_DEPTH],
		int (&Tsize)[MAX_DEPTH],
		int d)
{
#pragma HLS inline off
	Rsize[d] = Rsize[d-1];
	Tsize[d] = Tsize[d-1];

	for (int i = 0; i < BRAM; i++)
	{
#pragma HLS unroll
		for (int j = 0; j < MAX_INT/BRAM; j++)
		{
			WS[d][i][j] = WS[d-1][i][j];
		}
	}

	for (int i = 0; i < MAX_R_SIZE; i++)
	{
#pragma HLS unroll
		WR[d][i] = WR[d-1][i];
	}

	for (int i = 0; i < MAX_TARGET; i++)
	{
#pragma HLS unroll
		WT[d][i] = WT[d-1][i];
	}
}

void DFS(int _c_max, int depth, int _T[MAX_TARGET], int* _R, int* done)
{

	*done = 0;
	int_t c_max = (int_t)(_c_max>>1);
	int_t WR[MAX_DEPTH][MAX_R_SIZE] = {0};
#pragma HLS array_partition variable=WR dim=1 factor=2 cyclic
#pragma HLS array_partition variable=WR dim=2 complete
	bit_t WS[MAX_DEPTH][BRAM][MAX_INT/BRAM] = {0};
#pragma HLS array_partition variable=WS dim=2 complete
#pragma HLS array_partition variable=WS dim=1 factor=2 cyclic
	int_t WT[MAX_DEPTH][MAX_TARGET] = {0};
#pragma HLS array_partition variable=WT dim=1 factor=2 cyclic
#pragma HLS array_partition variable=WT dim=2 complete


	int_t R[MAX_R_SIZE] = {0};
#pragma HLS array_partition variable=R complete

	int Rsize[MAX_DEPTH];
	int Tsize[MAX_DEPTH];
	int_t ic_list[MAX_DEPTH];

	int sizeT = 0;

loopInitT:
	for (int i = 0; i < MAX_TARGET; i++)
	{
		WT[0][i] = (int_t)_T[i];
		sizeT = (_T[i] == MAX_INT) ? sizeT : (sizeT + 1);
	}
	WR[0][0] = (int_t)0;
loopInitWR:
	for (int i = 1; i < MAX_R_SIZE; i++)
		WR[0][i] = (int_t)-1;
	Rsize[0] = 1;
	Tsize[0] = sizeT;

	add_successors(c_max, WR, WS, 0, 0);


	synthesize(c_max, WR, WS, WT, Rsize, Tsize, 0);

	for (int i = 0; i < MAX_R_SIZE; i++)
#pragma HLS unroll factor=32
		R[i] = WR[0][i];

	int lb = sizeT + 2;
	int ub = (depth < MAX_DEPTH) ? (depth - 1) : (MAX_DEPTH - 1);
	int d = 0;
	ic_list[0] = 0;

WhileTrueLoop:
	while (true)
	{
		if (d > MAX_DEPTH)
			return;

		if (sizeT + d + 1 <= ub - 1)
		{
			int_t ic = find_intermediate_constant(WR, WS, ic_list, c_max, d);
			if (ic)
			{
				ic_list[d] = ic;
				d++;
				copy(WR, WS, WT, Rsize, Tsize, d);
				int tmp_size = Rsize[d];
				WR[d][tmp_size] = ic;
				Rsize[d] = tmp_size + 1;
				add_successors(c_max, WR, WS, d, ic);
				synthesize(c_max, WR, WS, WT, Rsize, Tsize, d);

				if (Tsize[d] == 0)
				{
loopWriteR:
					for (int i = 0; i < MAX_R_SIZE; i++)
//#pragma HLS unroll factor=32
						R[i] = WR[d][i];
					ub = Rsize[d] - 1;
					if (lb == ub)
						break;
					else
						d = ub - sizeT - 2;
				}
				else
				{
					if (CLB(WR, Rsize, WT, d) >= ub)
						d--;
					else
						ic_list[d] = 1;
				}
			}
			else
				d--;
		}
		else
			d--;
		if (d == -1)
			break;
	}
loopRes:
	for (int i = 0; i < MAX_R_SIZE; i++)
	{
		_R[i] = (int)R[i];
	}
	*done = 1;
}
