#include "krnl.hpp"

#ifndef SORT_HPP_
#define SORT_HPP_

void sort(unsigned int (&S)[MAX_S_SIZE], int size);

#endif
