#ifndef SYNTHESIZE_HPP_
#define SYNTHESIZE_HPP_

#include "krnl.hpp"

void synthesize(int_t c_max,
		int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int_t (&WT)[MAX_DEPTH][MAX_TARGET],
		int (&Rsize)[MAX_DEPTH],
		int (&Tsize)[MAX_DEPTH],
		int d);

#endif
