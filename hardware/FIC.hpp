#include "krnl.hpp"

#ifndef FIC_CPP_
#define FIC_CPP_

int_t find_intermediate_constant(int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int_t (&ic_list)[MAX_DEPTH],
		int_t c_max, int d);

#endif
