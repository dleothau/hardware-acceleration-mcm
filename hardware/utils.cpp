#include "utils.hpp"
#include "krnl.hpp"

int log2f(int _x)
{
#pragma HLS inline
	  int x = _x;

	  int cond1 = (x & 0xFFFF0000);
	  int y = (cond1 ? (y + 16) : y);
	  x = (cond1 ? (x>>16) : x);

	  int cond2 = (x & 0xFF00);
	  y = (cond2 ? (y + 8) : y);
	  x = (cond2 ? (x>>8) : x);

	  int cond3 = (x & 0xF0);
	  y = (cond3 ? (y + 4) : y);
	  x = (cond3 ? (x>>4) : x);

	  int cond4 = (x & 0xC);
	  y = (cond4 ? (y + 2) : y);
	  x = (cond4? (x>>2) : x);

	  int cond5 = (x & 0x00000002);
	  y = (cond5 ? (y + 1) : y);
	  return y;
}

int log2c(int _x)
{
#pragma HLS inline
	  int x = _x;
	  int y = 0;

	  int cond1 = (x & 0xFFFF0000);
	  y = (cond1 ? (y + 16) : y);
	  bit_t round_up = (cond1 && (x & 0x0000FFFF)) ? (bit_t)1 : (bit_t)0 ;
	  x = (cond1 ? (x>>16) : x);

	  int cond2 = (x & 0xFF00);
	  y = (cond2 ? (y + 8) : y);
	  round_up |= (cond2 && (x & 0x00FF)) ? (bit_t)1 : (bit_t)0 ;
	  x = (cond2 ? (x>>8) : x);

	  int cond3 = (x & 0xF0);
	  round_up |= (cond3 && (x & 0x0F)) ? (bit_t)1 : (bit_t)0 ;
	  y = (cond3 ? (y + 4) : y);
	  x = (cond3 ? (x>>4) : x);

	  int cond4 = (x & 0xC);
	  y = (cond4 ? (y + 2) : y);
	  round_up |= (cond4 && (x & 0x3)) ? (bit_t)1 : (bit_t)0 ;
	  x = (cond4? (x>>2) : x);

	  int cond5 = (x & 0x00000002);
	  round_up |= (cond5 && (x & 1)) ? (bit_t)1 : (bit_t)0 ;
	  y = (cond5 ? (y + 1) : y);

	  return (round_up ? (y+1) : y);
}

int odd(int x)
{
#pragma HLS inline
	x = (x & 0xffff) ? x : (x>>16);
	x = (x & 0xff) ? x : (x>>8);
	x = (x & 0xf) ? x : (x>>4);
	x = (x & 0x3) ? x : (x>>2);
	x = (x & 0x1) ? x : (x>>1);
	return x;
}


int compute_k_max(int c_max, int u, int v)
{
#pragma HLS inline
	return 1+(log2c(c_max + u) - log2f(v));
}


