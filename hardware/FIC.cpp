#include "FIC.hpp"

bool WR_contains(int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		int d, int_t ic)
{
	bool res = false;
	for (int i = 0; i < MAX_R_SIZE; i++)
#pragma HLS unroll
		res |= (WR[d][i] == ic);
	return res;
}

int_t find_intermediate_constant(int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int_t (&ic_list)[MAX_DEPTH],
		int_t c_max, int d)
{
#pragma HLS inline off

	int_t res = 0;
	loopFIC:
	for (int_t ic = MAX_INT-1; ic > 0; ic--)
		res = (((ic > ic_list[d]) &&
				(ic < c_max) &&
				WS[d][ic%BRAM][ic/BRAM] &&
				(!WR_contains(WR,d,ic))) ? ic : res);
	return res;

}
