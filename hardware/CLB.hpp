#include "krnl.hpp"
#include "nnzd.hpp"
#include "utils.hpp"
#include "sort.hpp"

#ifndef CLB_CPP_
#define CLB_CPP_

int CLB(int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		int (&Rsize)[MAX_DEPTH],
		int_t (&WT)[MAX_DEPTH][MAX_TARGET],
		int d);

#endif
