#include "nnzd.hpp"

unsigned int count_bits1(ap_uint<1> x)
{
#pragma HLS inline
	return (x ? 1 : 0);
}

unsigned int count_bits2(ap_uint<2> x)
{
#pragma HLS inline
	return count_bits1((ap_uint<1>)x) + count_bits1((ap_uint<1>)(x>>1));
}


unsigned int count_bits4(ap_uint<4> x)
{
#pragma HLS inline
	return count_bits2((ap_uint<2>)x) + count_bits2((ap_uint<2>)(x>>2));
}

unsigned int count_bits8(ap_uint<8> x)
{
#pragma HLS inline
	return count_bits4((ap_uint<4>)x) + count_bits4((ap_uint<4>)(x>>4));
}

unsigned int count_bits16(ap_uint<16> x)
{
#pragma HLS inline
	return count_bits8((ap_uint<8>)x) + count_bits8((ap_uint<8>)(x>>8));
}

unsigned int count_bits32(ap_uint<32> x)
{
#pragma HLS inline
	return count_bits16((ap_uint<16>)x) + count_bits16((ap_uint<16>)(x>>16));
}

unsigned int count_bits64(ap_uint<64> x)
{
#pragma HLS inline
	return count_bits32((ap_uint<32>)x) + count_bits32((ap_uint<32>)(x>>32));
}

unsigned int nnzd(int_t y)
{
#pragma HLS inline
	ap_uint<64> x = 2*y+1;
	ap_uint<64> tmp1 = x >> 1;
	ap_uint<64> tmp2 = x + tmp1;
	ap_uint<64> tmp3 = tmp1 ^ tmp2;
	ap_uint<64> csd = (tmp1 | tmp2) & tmp3;

	return count_bits64(csd);
}

