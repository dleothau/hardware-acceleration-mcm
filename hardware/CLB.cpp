#include "CLB.hpp"

int CLB(int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		int (&Rsize)[MAX_DEPTH],
		int_t (&WT)[MAX_DEPTH][MAX_TARGET],
		int d)
{
#pragma HLS inline off
	unsigned int S[MAX_S_SIZE];
#pragma HLS array_partition variable=S complete
	int index = Rsize[d];

	for (int i = 1; i < MAX_R_SIZE; i++)
	{
		S[i] = nnzd(WR[d][i]);
	}
	for (int i = 1; i < MAX_TARGET; i++)
	{
		int_t t = WT[d][i];
		S[index] = nnzd(t);
		if (t != MAX_INT)
			index++;
	}
	sort(S, index);

	int lb = log2c(S[0]);


loopComputeLb:
	for (int i = 1; i < MAX_S_SIZE; i++)
	{
		lb += ((i < index) ?
				((S[i-1] == S[i]) ? 1 : (log2c(S[i]) - log2f(S[i-1]))) :
				0);
	}

	return lb;
}

