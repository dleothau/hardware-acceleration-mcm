#include "krnl.hpp"

#include "utils.hpp"

#ifndef ADD_SUCCESSORS_HPP_
#define ADD_SUCCESSORS_HPP_

void add_successors(int_t c_max,
		int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int d, int_t x);

#endif
