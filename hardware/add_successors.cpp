#include "add_successors.hpp"

void comb_logic(int_t (&new_suc)[NEW_SUCC_SIZE],
		int_t (&mux)[BRAM],
		bit_t (&ok)[NEW_SUCC_SIZE],
		bit_t (&en)[BRAM])
{
#pragma HLS inline
	for (int_t i = 0; i < BRAM; i++)
	{
#pragma HLS unroll
		int_t tmp = -1;
		for (int j = 0; j < NEW_SUCC_SIZE; j++)
#pragma HLS unroll
			if (ok[j] && ((new_suc[j]%BRAM) == i))
			{
				tmp = j;
			}
		mux[i] = tmp;
		en[i] = ((tmp == ((int_t)-1)) ? (bit_t)0 : (bit_t)1);

		if (tmp != -1)
			ok[tmp] = (bit_t)0;
	}
}

void add_back(bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int d,
		int_t (&new_suc)[NEW_SUCC_SIZE],
		bit_t (&ok)[NEW_SUCC_SIZE])
{
#pragma HLS inline off

loopAddBack:
	for (int __j = 0; __j < NEW_SUCC_SIZE; __j++)
	{
#pragma hls pipeline off
#pragma HLS unroll factor=1

		bit_t cond = 0;
		for (auto i = 0; i < NEW_SUCC_SIZE; i++)
#pragma HLS unroll
			cond |= ok[i];
		if (!cond)
			break;

		bit_t en[BRAM] = {0};
		int_t mux[BRAM];
	#pragma HLS array_partition variable=en complete
	#pragma HLS array_partition variable=mux complete

		comb_logic(new_suc, mux, ok, en);

	loopWriteWS:
		for (auto i = 0; i < BRAM; i++)
		{
	#pragma HLS unroll
			int_t addr = new_suc[mux[i]]/BRAM;
			if (en[i])
				WS[d][i][addr] = (bit_t)1;
		}
	}
}

void add_successors(int_t c_max,
		int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int d, int_t x)
{
#pragma HLS inline off

loopSucessor:
	for (auto z = 0; z < MAX_R_SIZE; z++)
	{
#pragma HLS PIPELINE off
		int_t new_suc[NEW_SUCC_SIZE];
	#pragma HLS array_partition variable=new_suc dim=0 complete
		bit_t ok[NEW_SUCC_SIZE] = {0};
	#pragma HLS array_partition variable=ok complete

		int_t y = WR[d][z];
		if (y != -1)
		{

			int_t u = (x < y) ? (2*y+1) : (2*x+1);
			int_t v = (x < y) ? (2*x+1) : (2*y+1);

			ap_uint<64> s = odd(u+v)>>1;
			new_suc[0] = (int_t)s;
			new_suc[1] = (int_t)(odd(u-v)>>1);
			ok[z * (4*MAX_BW-2) + 0] =
					ok[z * (4*MAX_BW-2) + 1] =
							(s < c_max) ? (bit_t)1 : (bit_t)0;

			for (int k = 1; k < MAX_BW; k++)
			{
#pragma HLS unroll
				s = (u<<k)+v;
				s >>= 1;
				new_suc[4 * (k-1) + 2] = (int_t)s;
				ok[4 * (k-1) + 2] = (s < c_max) ? (bit_t)1 : (bit_t)0;

				s = u+(v<<k);
				s >>= 1;
				new_suc[4 * (k-1) + 3] = (int_t)s;
				ok[4 * (k-1) + 3] = (s < c_max) ? (bit_t)1 : (bit_t)0;

				s = (u<<k)-v;
				s >>= 1;
				new_suc[4 * (k-1) + 4] = (int_t)s;
				ok[4 * (k-1) + 4] = (s < c_max) ? (bit_t)1 : (bit_t)0;

				s = (u > (v<<k)) ? (u - (v<<k)) : ((v<<k) - u);
				s >>= 1;
				new_suc[4 * (k-1) + 5] = (int_t)s;
				ok[4 * (k-1) + 5] = (s < c_max) ? (bit_t)1 : (bit_t)0;

			}
			add_back(WS, d, new_suc, ok);
		}

	}
}
