#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS


#include <cstddef>
#include <vector>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <CL/cl2.hpp>

#include "krnl.hpp"
#include "approximateSearch.hpp"
#include "one.hpp"

#define OCL_CHECK(error, call)                                                                   \
		call;                                                                                        \
		if (error != CL_SUCCESS) {                                                                   \
			printf("%s:%d Error calling " #call ", error code is: %d\n", __FILE__, __LINE__, error); \
			exit(EXIT_FAILURE);                                                                      \
		}



inline int abs(int x)
{
	return ((x>0) ? x : -x);
}


std::vector<cl::Device> get_xilinx_devices()
				{
	size_t i;
	cl_int err;
	std::vector<cl::Platform> platforms;
	err = cl::Platform::get(&platforms);
	cl::Platform platform;
	for (i = 0; i < platforms.size(); i++)
	{
		platform = platforms[i];
		std::string platformName = platform.getInfo<CL_PLATFORM_NAME>(&err);
		if (platformName == "Xilinx")
		{
			std::cout << "INFO: Found Xilinx Platform" << std::endl;
			break;
		}
	}
	if (i == platforms.size())
	{
		std::cout << "ERROR: Failed to find Xilinx platform" << std::endl;
		exit(EXIT_FAILURE);
	}

	//Getting ACCELERATOR Devices and selecting 1st such device
	std::vector<cl::Device> devices;
	err = platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &devices);
	return devices;
				}



char *read_binary_file(const std::string &xclbin_file_name, unsigned &nb)
{
	if (access(xclbin_file_name.c_str(), R_OK) != 0)
	{
		printf("ERROR: %s xclbin not available please build\n", xclbin_file_name.c_str());
		exit(EXIT_FAILURE);
	}
	//Loading XCL Bin into char buffer
	std::cout << "INFO: Loading '" << xclbin_file_name << "'\n";
	std::ifstream bin_file(xclbin_file_name.c_str(), std::ifstream::binary);
	bin_file.seekg(0, bin_file.end);
	nb = bin_file.tellg();
	bin_file.seekg(0, bin_file.beg);
	char *buf = new char[nb];
	bin_file.read(buf, nb);
	std::cout << std::endl << std::endl;
	std::cout << buf << std::endl;
	std::cout << std::endl << std::endl;
	return buf;
}

int main(int argc, char** argv)
{
	auto start = std::chrono::high_resolution_clock::now();
	if (argc < 3)
	{
		std::cerr << "usage: " << argv[0] << "[constant]+ [xclbin]" << std::endl;
		return 1;
	}

	/* Init OpenCL */

	std::string xclbinFilename = argv[argc-1];

	std::vector<cl::Device> devices;
	cl::Device device;
	cl_int err;
	cl::Context context;
	cl::Program program;
	std::vector<cl::Platform> platforms;
	bool found_device = false;
	cl::Kernel krnl;
	cl::CommandQueue q;


	cl::Platform::get(&platforms);
	for(size_t i = 0; (i < platforms.size() ) & (found_device == false) ;i++){
		cl::Platform platform = platforms[i];
		std::string platformName = platform.getInfo<CL_PLATFORM_NAME>();
		if ( platformName == "Xilinx"){
			devices.clear();
			platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &devices);
			if (devices.size()){
				device = devices[0];
				found_device = true;
				break;
			}
		}
	}
	if (found_device == false){
		std::cout << "Error: Unable to find Target Device "
				<< device.getInfo<CL_DEVICE_NAME>() << std::endl;
		return EXIT_FAILURE;
	}
	OCL_CHECK(err, context = cl::Context(device, NULL, NULL, NULL, &err));
	OCL_CHECK(err, q = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &err));

	std::cout << "INFO: Reading " << xclbinFilename << std::endl;
	FILE* fp;
	if ((fp = fopen(xclbinFilename.c_str(), "r")) == nullptr) {
		printf("ERROR: %s xclbin not available please build\n", xclbinFilename.c_str());
		exit(EXIT_FAILURE);
	}
	// Load xclbin
	std::cout << "Loading: '" << xclbinFilename << "'\n";
	std::ifstream bin_file(xclbinFilename, std::ifstream::binary);
	bin_file.seekg (0, bin_file.end);
	unsigned nb = bin_file.tellg();
	bin_file.seekg (0, bin_file.beg);
	char *buf = new char [nb];
	bin_file.read(buf, nb);

	// Creating Program from Binary File
	cl::Program::Binaries bins;
	bins.push_back({buf,nb});
	devices.resize(1);
	std::vector<cl_int> binaryStatus;
	OCL_CHECK(err, program = cl::Program(context, devices, bins, &binaryStatus, &err));
	OCL_CHECK(err , krnl = cl::Kernel(program,"DFS", &err));


	/* void DFSearch(int _c_max, int depth, int _T[MAX_INT/32], int _R[MAX_INT/32], int* done) */

	/* Create Buffers */

	cl::Buffer buf_T(context, CL_MEM_READ_WRITE, sizeof(int) * MAX_TARGET, NULL, NULL);
	cl::Buffer buf_R(context, CL_MEM_READ_WRITE, sizeof(int) * MAX_R_SIZE, NULL, NULL);
	cl::Buffer buf_done(context, CL_MEM_READ_WRITE, sizeof(int), NULL, NULL);

	int* T = (int*) q.enqueueMapBuffer(buf_T, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(int)* MAX_TARGET);
	int* R = (int*) q.enqueueMapBuffer(buf_R, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(int)* MAX_R_SIZE);
	int* done = (int*) q.enqueueMapBuffer(buf_done, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(int));


	/* init buffer */

	int bw = 0;
	std::cout << "target: ";
	for (int i = 1; i < argc-1; i++)
	{
		int val = odd(atoi(argv[i]));
		int tmp = log2c(val);
		if (tmp > bw)
		{
			bw = tmp;
			c_max = (1<<(bw+1));
		}
		if (val > 1)
		{
			std::cout << val << " ";
			g_target.insert(val);
		}
		else
			std::cout << "--" << val << "-- ";
	}

	std::cout << std::endl;

	if (bw < 19)
	{
		real_set_t o = {1};
		real_set_t os = one_succ[bw+1];
		realisation_set_t tmp_set(o, os);
		g_one = tmp_set;
	}
	else
		g_one.insert(1);

	real_set_t real = approximate_search(g_target);

	std::cout << "approx: ";
	for (auto x : real)
		std::cout << x << " ";
	std::cout << std::endl;
	int depth = real.size()+2;
	std::cout << "bw: " << bw << std::endl
			<< "c_max: " << c_max << std::endl
			<< "depth: " << depth << std::endl;
	if (real.size() - 1 <= g_target.size() + 2)
	{
		return 0;
	}

	for (int i = 0; i < MAX_TARGET; i++)
		T[i] = MAX_INT;

	int index = 0;
	for (auto x : g_target)
		T[index++] = (x>>1);

	krnl.setArg(0, c_max);
	krnl.setArg(1, depth);
	krnl.setArg(2, buf_T);
	krnl.setArg(3, buf_R);
	krnl.setArg(4, buf_done);
	/* void DFS(int _c_max, int depth, int _T[MAX_TARGET], int _R[MAX_R_SIZE], int* done) */
	q.enqueueMigrateMemObjects({buf_T}, 0);
	q.finish();

	q.enqueueTask(krnl);
	q.finish();

	q.enqueueMigrateMemObjects({buf_R, buf_done}, CL_MIGRATE_MEM_OBJECT_HOST);
	q.finish();

	std::cout << *done << std::endl;

	if (*done == 0)
	{
		std::cout << "Can't be done with a max depth of " << MAX_DEPTH << std::endl;
	}

	std::cout << "result: ";
	for (int i = 0; i < MAX_R_SIZE; i++)
		if (R[i] != -1)
			std::cout << 1+2*R[i] << " ";
	std::cout << std::endl;

	q.enqueueUnmapMemObject(buf_done, done);
	q.enqueueUnmapMemObject(buf_T, T);
	q.enqueueUnmapMemObject(buf_R, R);

	auto elapsed = std::chrono::high_resolution_clock::now() - start;
	long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(
			elapsed).count();
	std::cout << "time: " << microseconds << "µs." << std::endl;

	return 0;
}







