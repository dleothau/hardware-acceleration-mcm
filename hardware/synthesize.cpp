#include "synthesize.hpp"
#include "add_successors.hpp"


void synthesize(int_t c_max,
		int_t (&WR)[MAX_DEPTH][MAX_R_SIZE],
		bit_t (&WS)[MAX_DEPTH][BRAM][MAX_INT/BRAM],
		int_t (&WT)[MAX_DEPTH][MAX_TARGET],
		int (&Rsize)[MAX_DEPTH],
		int (&Tsize)[MAX_DEPTH],
		int d)
{
#pragma HLS inline off
	int sizeToAdd = 0;
	int tmp_size = Rsize[d];
dowhileLoop:
	for (int i = 0; i < MAX_TARGET; i++)
	{
#pragma HLS pipeline off
		bit_t isadded = (bit_t)0;
		loopSynthesize:
		for (int k = 0; k < MAX_TARGET; k++)
		{
#pragma HLS pipeline off
			int_t t = WT[d][k];
			if ((WT[d][k] != MAX_INT) && WS[d][t%BRAM][t/BRAM])
			{
				isadded = (bit_t)1;
				sizeToAdd++;
				WR[d][tmp_size] = t;
				tmp_size++;

				add_successors(c_max, WR, WS, d, t);

				WT[d][k] = MAX_INT;

			}
		}
		if (isadded == (bit_t)0)
			break;
	}
	Tsize[d] -= sizeToAdd;
	Rsize[d] = tmp_size;
}

