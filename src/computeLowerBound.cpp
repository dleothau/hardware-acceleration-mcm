#include <cmath>
#include<boost/sort/spreadsort/spreadsort.hpp>

#include "computeLowerBound.hpp"
#include "CSD.hpp"

uint32_t compute_lower_bound(realisation_set_t& R, target_set_t& T)
{
  int size = R.size() + T.size() - 1; 
  std::vector<uint32_t> S;
  S.reserve(size);
  for (auto b : R)
    if (b != 1)
      S.push_back(number_non_zero_digit(b));
  for (auto b : T)
    S.push_back(number_non_zero_digit(b));

  boost::sort::spreadsort::spreadsort(S.begin(), S.end());
  auto index = std::distance(S.begin(), std::unique(S.begin(), S.end()));
  uint32_t lb = log2c(*S.begin()) + (size - index);

  std::vector<double> logS;
  logS.resize(index);
  for (int i = 0; i < index; i++)
    logS[i] = log2(S[i]);
  
  for (int i = 1; i<index; i++)
  {
    lb += ceil(logS[i] - logS[i-1]);
  }
  return lb;
}
