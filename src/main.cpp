#include <iostream>

#include "approximateSearch.hpp"
#include "DFSearch.hpp"
#include "one.hpp"

int abs(int x)
{
  if (x<0)
    return -x;
  return x;
}

int main(int argc, char** argv)
{
  unsigned int depth = ~0;
  bw = 0;
  std::vector<uint32_t> targs;
  for (int i = 1; i < argc; i++)
  {
    int val = odd(atoi(argv[i]));
    int tmp = log2c(val);
    if (tmp > bw)
    {
      bw = tmp;
      c_max = (1<<(bw+1));
    }
    if (val > 1)
      g_target.insert(val);
  }
  std::cout << "c_max: " << c_max << std::endl;
  if (bw < 19)
  {
    real_set_t o = {1};
    real_set_t os = one_succ[bw+1];
    realisation_set_t tmp_set(o, os);
    g_one = tmp_set;
  }
  else
    g_one.insert(1);

  for (auto x: g_one.successors)
    g_one_suc.insert(x);
  
  std::cout << "target: ";
  for (auto x : g_target)
  {
    std::cout << x << " ";
  }
  std::cout << std::endl;
    
  real_set_t R = approximate_search(g_target);
  uint32_t Rsize = R.size();

  if (g_target.size() + 2 < Rsize - 1)
  {
    R = DFSearch(g_target, Rsize+1);
  }
  
  std::cout << "cost: " << R.size() - 1 << std::endl;
  
  for (auto x : R)
  {
    std::cout << x << " ";
  }
  std::cout << std::endl;
  return 0;

}
