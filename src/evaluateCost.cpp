#include "evaluateCost.hpp"
#include "SCC.hpp"

uint32_t evaluate_cost(target_set_t& B)
{
  uint32_t cost = 0;
  for (auto b : B)
    cost += single_coefficient_cost(b);
  return cost;
}
