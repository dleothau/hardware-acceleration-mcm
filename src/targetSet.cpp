#include "targetSet.hpp"

target_set_t g_target;
targ_set_t g_empty_set;

void target_set_t::insert(uint32_t x)
{
  this->set.insert(x);
}

targ_set_t::iterator target_set_t::erase(targ_set_t::iterator it)
{
  it = this->set.erase(it);
  return it;
}


void target_set_t::erase(uint32_t x)
{
  this->set.erase(x);
}

bool target_set_t::contains(uint32_t x)
{
  return this->set.contains(x);
}

bool target_set_t::empty(void)
{
  return this->set.empty();
}


size_t target_set_t::size(void)
{
  return this->set.size();
}


targ_set_t::iterator target_set_t::begin(void)
{
  return this->set.begin();
}

targ_set_t::iterator target_set_t::end(void)
{
  return this->set.end();
}


target_set_t& target_set_t::operator=(target_set_t& other)
{
  this->set = other.set;
  return *this;
}
