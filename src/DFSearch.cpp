#include "computeLowerBound.hpp"
#include "DFSearch.hpp"
#include "synthesize.hpp"


real_set_t DFSearch(target_set_t T, uint32_t depth)
{
  realisation_set_t R;
  int lb = T.size() + 2;
  int ub = depth - 1;
  int d = 0;
  std::map<uint32_t,uint32_t> ic_list{{0,1}};

  std::map<uint32_t, realisation_set_t> WR;
  std::map<uint32_t, target_set_t> WT;
  
  auto tmp = get_one();
  WR[0] = tmp;
  WT[0] = T;

  synthesize(WR[0], WT[0]);
  
  while (true)
  {
    if (T.size() + d + 1 <= ub - 1)
    {
      auto ic = WR[d].find_intermediate_constant(ic_list[d], d);
      if (ic)
      {
	
	ic_list[d] = ic;
	d++;
	
	WR[d] = WR[d-1];
	WT[d] = WT[d-1];
	WR[d].insert(ic);
	synthesize(WR[d], WT[d]);

	if (WT[d].empty())
	{
	  R = WR[d];
	  ub = R.size() - 1;
	  if (lb == ub)
	  {
	    return R.set;
	  }
	  else
	    d = ub - T.size() - 2;
	}
	else
	{
	  if (compute_lower_bound(WR[d], WT[d]) >= ub)
	    d--;
	  else
	    ic_list[d] = 1;
	}
      }
      else
	d--;
    }
    else
      d--;
    if (d == -1)
      return R.set;
  }
}
