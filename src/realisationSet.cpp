#include<boost/sort/spreadsort/spreadsort.hpp>
#include <numeric>

#include "one.hpp"
#include "realisationSet.hpp"
#include "synthesize.hpp"

real_set_t g_empty;

class setHash
{
private:
  std::hash<unsigned int> base;
public:
  std::size_t operator()(const real_set_t& s) const 
    {
      return base(std::accumulate(s.begin(), s.end(),0, [this](auto x, auto y){ return x ^ y*y; }));
    }
};

std::unordered_set<real_set_t, setHash> explored;

void realisation_set_t::add_successor(uint32_t x)
{
  this->tmp_suc.reserve(this->tmp_suc.size() + this->set.size()*(4*bw+6));
  for (auto y : this->set)
  {
    auto u = x > y ? x : y;
    auto v = x > y ? y : x;

    uint32_t s = odd(u+v);
    if (s < c_max)
    {
      this->tmp_suc.push_back(s);
      this->tmp_suc.push_back(odd(u-v));
    }

    auto k_max = compute_k_max(u, v);
    
    for (auto k = 1; k < k_max; k++)
    {
      s = (u<<k) + v;
      if (s < c_max)
	this->tmp_suc.push_back(s);

      s = u + (v<<k);
      if (s < c_max)
      	this->tmp_suc.push_back(s);

      s = (u<<k) - v;
      if (s < c_max)
      	this->tmp_suc.push_back(s);

      s = (u > (v<<k)) ? (u - (v<<k)) : ((v<<k) - u);
      if (s < c_max)
      	this->tmp_suc.push_back(s);
    }
  }

  boost::sort::spreadsort::spreadsort(this->tmp_suc.begin(), this->tmp_suc.end());
  this->tmp_suc.erase(std::unique(this->tmp_suc.begin(), this->tmp_suc.end()), this->tmp_suc.end());
  
  real_set_t dest;
  dest.resize(this->tmp_suc.size() + this->successors.size());
  std::merge(this->successors.begin(), this->successors.end(), this->tmp_suc.begin(), this->tmp_suc.end(), dest.data());
  this->successors.swap(dest);    

  this->successors.erase(std::unique(this->successors.begin(), this->successors.end()), this->successors.end());
  this->tmp_suc.clear();
}

void realisation_set_t::add_successor(uint32_t x, realisation_set_t& other)
{
  for (auto y : other.set)
  {
    auto u = x > y ? x : y;
    auto v = x > y ? y : x;

    uint32_t s = odd(u+v);
    if (s < c_max)
    {
      this->tmp_suc.push_back(s);
      this->tmp_suc.push_back((odd(u-v)));
    }

    auto k_max = compute_k_max(u, v);
    
    for (auto k = 1; k < k_max; k++)
    {
      s = (u<<k) + v;
      if (s < c_max)
	this->tmp_suc.push_back(s);

      s = u + (v<<k);
      if (s < c_max)
      	this->tmp_suc.push_back(s);

      s = (u<<k) - v;
      if (s < c_max)
      	this->tmp_suc.push_back(s);

      s = (u > (v<<k)) ? (u - (v<<k)) : ((v<<k) - u);
      if (s < c_max)
      	this->tmp_suc.push_back(s);
    }
  }
  this->add_successor(x);
}

void realisation_set_t::insert(uint32_t x)
{
  this->set.insert(std::upper_bound(this->set.begin(), this->set.end(), x), x);
  this->add_successor(x);
}

void realisation_set_t::insert(uint32_t x, realisation_set_t& other)
{
  this->set.insert(std::upper_bound(this->set.begin(), this->set.end(), x), x);
  this->add_successor(x, other);
}


void realisation_set_t::erase(uint32_t x)
{
  this->set.erase(std::remove(std::begin(this->set), std::end(this->set), x), std::end(this->set));
}


bool realisation_set_t::empty(void)
{
  return this->set.empty();
}

bool realisation_set_t::contains(uint32_t x)
{
  return std::binary_search(this->set.begin(), this->set.end(), x);
}


bool realisation_set_t::successor(uint32_t x)
{
  return std::binary_search(this->successors.begin(), this->successors.end(),x);
}

size_t realisation_set_t::size(void)
{
  return this->set.size();
}


real_set_t::iterator realisation_set_t::begin(void)
{
  return this->set.begin();
}

real_set_t::iterator realisation_set_t::end(void)
{
  return this->set.end();
}


real_set_t::iterator realisation_set_t::begin_successor(void)
{
  return this->successors.begin();
}

real_set_t::iterator realisation_set_t::end_successor(void)
{
  return this->successors.end();
}


realisation_set_t& realisation_set_t::operator=(realisation_set_t& other)
{
  this->set = other.set;
  this->successors = other.successors;
  return *this;
}

realisation_set_t get_one(void)
{
  return g_one;
}


target_set_t realisation_set_t::to_target(void)
{
  targ_set_t tmp(this->set.begin(), this->set.end());
  return target_set_t(tmp);
}

real_set_t realisation_set_t::remove_redundant(void)
{
  auto one = get_one();
  auto A = this->to_target();
  std::unordered_set<uint32_t> to_remove;
  for (auto x : this->set)
    if ((x != 1) &&
	!(g_target.contains(x)))
    {
      A.erase(x);
      if (synthesize_no_change(one, A))
	to_remove.insert(x);
      else
	A.insert(x);
    }
  for (auto x : to_remove)
    this->erase(x);
  return this->set;
}


uint32_t realisation_set_t::find_intermediate_constant(uint32_t ic)
{
  if (this->empty())
    return 0;
  auto it = std::upper_bound(this->successors.begin(),
			     this->successors.end(),
			     ic);
  auto it2 = std::upper_bound(this->set.begin(), this->set.end(), ic);
  for (; it != this->successors.end(); it++)
  {
    it2 = std::lower_bound(it2, this->set.end(), *it);
    if ((it2 == this->set.end()) || (*it != *it2))
    {
      return *it;
    }
  }
  return 0;
}

uint32_t realisation_set_t::find_intermediate_constant_symmetry_check(uint32_t ic)
{
  if (this->empty())
    return 0;
  real_set_t set_cpy(this->set.begin(), this->set.end());
  
  auto it = std::upper_bound(this->successors.begin(),
			     this->successors.end(),
			     ic);
  auto it2 = std::upper_bound(this->set.begin(), this->set.end(), ic);
  for (; it != this->successors.end(); it++)
  {
    it2 = std::lower_bound(it2, this->set.end(), *it);
    if ((it2 == this->set.end()) || (*it != *it2))
    {
      real_set_t set_cpy2 = set_cpy;

      
      set_cpy2.push_back(*it);
      boost::sort::spreadsort::spreadsort(set_cpy2.begin(), set_cpy2.end());
      
      if (!explored.contains(set_cpy2))
      {
	explored.insert(set_cpy2);
	return *it;
      }
    }
  }
  return 0;
}

uint32_t realisation_set_t::find_intermediate_constant(uint32_t ic, int d)
{
  if (d < MAX_PROF_SYMMETRY_CHECK)
    return this->find_intermediate_constant_symmetry_check(ic);
  else
    return this->find_intermediate_constant(ic);
}
