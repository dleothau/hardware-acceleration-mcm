#include "approximateSearch.hpp"
#include "evaluateCost.hpp"
#include "synthesize.hpp"

real_set_t approximate_search(target_set_t T)
{  
  auto R = get_one();
  synthesize(R, T);

  if (T.empty())
    return R.set;
  
  while (true)
  {
    std::map<uint32_t, uint32_t> cost_mirror;
    for (auto it = R.successors.rbegin(); it != R.successors.rend(); it++)
    {
      auto j = *it;
      if (!R.contains(j))
      {
	realisation_set_t A(R);
	A.insert(j);
	target_set_t C(T);
	synthesize(A,C);
	
	if (C.empty())
	{
	  return A.remove_redundant();
	}
	else
	{
	  cost_mirror[evaluate_cost(C)] = j;
	}
      }
    }
    auto tmp = cost_mirror.begin()->second;
    R.insert(tmp);
    synthesize(R, T);
  }
}
