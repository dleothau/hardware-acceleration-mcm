#include "CSD.hpp"
#include "int.hpp"
#include "SCC.hpp"

const char* SCC_array =
#include "SCC"
;

uint32_t single_coefficient_cost(uint32_t b)
{
  b = odd(b);
  if (b < MAX_COMPUTED)
    return (uint32_t)(SCC_array[b>>1] - '0');
  else
    return number_non_zero_digit(b);
}

