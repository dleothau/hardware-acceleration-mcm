#include "synthesize.hpp"


void synthesize(realisation_set_t& R, target_set_t& T)
{
  bool isadded;
  do
  {
    isadded = false;
    for (auto it = T.begin(); it != T.end();)
    {
      if (R.successor(*it))
      {
	isadded = true;
	R.insert(*it);
	it = T.erase(it);
      }
      else
	it++;
    }
  } while (isadded);
}

bool synthesize_no_change(realisation_set_t& R, target_set_t& T)
{
  realisation_set_t nR;
  target_set_t nT;
  bool isadded;
  do
  {
    isadded = false;
    for (auto it = T.begin(); it != T.end();)
    {
      if (R.successor(*it) || nR.successor(*it))
      {
	isadded = true;
	nR.insert(*it, R);
	nT.insert(*it);
	it = T.erase(it);
      }
      else
	it++;
    }
  } while (isadded);
  bool res = T.empty();
  for (auto x : nT)
    T.insert(x);
  return res;
}
