#include "int.hpp"

uint32_t c_max = 0;
uint32_t bw = 0;

uint64_t popcount(uint64_t x)
{
  x = x - ((x >> 1) & 0x5555555555555555);
  x = (x & 0x3333333333333333) + ((x>>2) & 0x3333333333333333);
  x = (x + (x >> 4)) & 0x0F0F0F0F0F0F0F0F;
  return (x * 0x0101010101010101) >> 56;
}

uint32_t popcount(uint32_t x)
{
  x = x - ((x >> 1) & 0x55555555);
  x = (x & 0x33333333) + ((x>>2) & 0x33333333);
  x = (x + (x >> 4)) & 0x0F0F0F0F;
  return (x * 0x01010101) >> 24;
}

uint32_t log2c(uint32_t x)
{
  uint32_t round_up = (popcount(x) == 1) ? -1 : 0;

  x |= (x >> 1);
  x |= (x >> 2);
  x |= (x >> 4);
  x |= (x >> 8);
  x |= (x >> 16);
  return (popcount(x) + round_up);
}


uint32_t log2f(uint32_t x)
{
  x |= (x >> 1);
  x |= (x >> 2);
  x |= (x >> 4);
  x |= (x >> 8);
  x |= (x >> 16);
  return (popcount(x) - 1);
}


uint32_t odd(uint32_t x)
{
  while (x && !(x & 1))
    x >>= 1;
  return x;
}

int compute_k_max(uint32_t u, uint32_t v)
{
  return (((int)log2c(c_max + u)) - ((int)log2f(v)));
}
