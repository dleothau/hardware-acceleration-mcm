#include "CSD.hpp"
#include "int.hpp"

const char nnzd_array[] =
#include "non_zero_array"
;

uint32_t number_non_zero_digit(uint32_t x)
{
  if (x < MAX_COMPUTED)
    return 1+(nnzd_array[x] - '0');

  uint64_t y = (uint64_t)x;
  
  uint64_t tmp1 = y >> 1;
  uint64_t tmp2 = y + tmp1;
  uint64_t tmp3 = tmp1 ^ tmp2;
  uint64_t csd = (tmp2 | tmp1) & tmp3;
  
  uint64_t res = popcount(csd);

  return (uint32_t)res;
}

