#include "synthesize.hpp"
#include "thread.hpp"

#include <mutex>
#include <condition_variable>
#include <semaphore>
#include <iostream>
#include <limits.h>
#include <thread>
#include <chrono>

std::mutex mut_res, mut_stack;
real_set_t res;
bool isRes = false;
stack_t global_stack;


void declareRealisation(realisation_set_t& R)
{
  mut_res.lock();
  if (!isRes || (R.size() < res.size()))
  {
    res = R.set;
    isRes = true;
  }
  mut_res.unlock();
}

int getMinSize(void)
{
  mut_res.lock();
  bool tmpCond = isRes;
  int size = res.size();
  mut_res.unlock();
  
  if (tmpCond)
    return size;
  else
    return INT_MAX;
}


void threadFun(void)
{
  stack_elt_t* elt;
  bool forward = false;
  stack_t stack;
  bool alive = true;

  while (alive)
  {
    if (stack.empty())
    {
      mut_stack.lock();

      if (global_stack.empty())
	alive = false;
      else
      {
	stack.push(global_stack.top());
	global_stack.pop();
      }
      
      mut_stack.unlock();
    }
    else
    {
      if (!forward)
      {
	elt = stack.top();
	stack.pop();
      }
      forward = false;
      if ((getMinSize() > elt->T.size()) && (elt->T.size() == 0))
      {
	declareRealisation(elt->R);
	delete elt;
      }
      else if (getMinSize() > (elt->R.size() + elt->T.size() + 1))
      {
	int ic = elt->R.find_intermediate_constant(elt->last_ic);
	if (ic)
	{
	  stack_elt_t* next_elt = new stack_elt_t;
	  realisation_set_t tmpR = elt->R;
	  next_elt->R = tmpR;
	  target_set_t tmpT = elt->T;
	  next_elt->T = tmpT;
	  next_elt->last_ic = ic;
	  elt->last_ic = 1;
	  elt->R.insert(ic);
	  synthesize(elt->R, elt->T);
        
	  stack.push(next_elt);
	  forward = true;
	}
      }
      else
      {
	delete elt;
      }
    }
  }
}

void init(stack_t& stack_in, stack_t& stack_out)
{
  while (!stack_in.empty())
  {
    stack_elt_t* elt = stack_in.top();
    stack_in.pop();

    if (elt->T.size() == 0)
    {
      declareRealisation(elt->R);
      delete elt;
    }
    else if (getMinSize() > (elt->R.size() + elt->T.size() + 1))
    {
      int ic = elt->R.find_intermediate_constant(elt->last_ic);
      if (ic)
      {
	stack_elt_t* next_elt = new stack_elt_t;
	realisation_set_t tmpR = elt->R;
	next_elt->R = tmpR;
	target_set_t tmpT = elt->T;
	next_elt->T = tmpT;
	next_elt->last_ic = ic;
	elt->last_ic = 1;
	elt->R.insert(ic);
	synthesize(elt->R, elt->T);
	
	stack_out.push(next_elt);
	stack_out.push(elt);
      }
    }
  }
}

real_set_t multiThread(target_set_t T)
{
  stack_elt_t* elt = new stack_elt_t();
  realisation_set_t one = get_one();
  elt->R = one;
  elt->T = T;
  synthesize(elt->R, elt->T);
  elt->last_ic = 1;

  stack_t stack1, stack2;
  stack1.push(elt);

  for (int i = 0; i < NUM_INIT/2; i++)
  {
    init(stack1, stack2);
    init(stack2, stack1);
  }
  if (NUM_INIT%2)
  {
    init(stack1, stack2);
    global_stack.swap(stack2);
  }
  else
    global_stack.swap(stack1);
  
  std::thread th[NUM_THREAD];
  for (int i = 0; i < NUM_THREAD; i++)
    th[i] = std::thread(threadFun);
  for (int i = 0; i < NUM_THREAD; i++)
    th[i].join();
  return res;
}
